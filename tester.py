#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Tester API client for a hw tester based LabJack-but-not-quite.

Actually written to Javier's own MiniJack API.

(c) 2013-2015 Javier Candeira and Planet Innovation.
"""

from functools import partial
from itertools import chain

from constants import *

class Tester():
    """Example with an Analog In signal
    --------------------------------
    So, for code that reads like this:
    >>> tester.someboard.a_signal_name.read()
    The tester ends up making this call using its internal _description :
    >>> vcc = tester.read(someboard, a_signal_name)
    Then the read method is executed, and it:
        - Figures out whether the pin can be read 
            - has it been mapped in the description?
            - is its rw status READ?
        - And either:
            - Throws an exception, or
            - builds the call for its internal device instance of LabJack, 
              where it arrives already mapped to analog/digital, 
              and to the proper pin number, etc:
    >>> vcc = mj.getAIState(3)

    ...or that's the call it would make if it weren't because we're not really
    calling to the labjack interface, but to a modified and simplified MiniJack
    one.

    But same difference.

    Digital Out: same deal
    ----------------------
    So, something like this:
    >>> testerAPI.someboard.a_signal.write(OFF)
    The call at ths point will be something like
    >>> lj_device.setDOState(1, 0)

    Simplified calls: same as above.

    """
    def __init__(self, description, devices):
        self.devices = devices
        self.description = description

    def __getattr__(self, method):
        """
        Generate internal lightweight object that builds the read/write call.
        
        Callbuilder is agnostic as to the actual configuration of the hardware.
        It will happily build illegal calls. It's up to the Tester object's 
        'read' and 'write' methods to raise exceptions if the call is not good.

        This knowledge of which calls are good or bad is delegated to the testerAPI object.

        """
        return Callbuilder(method, read=self.read, write=self.write)

    def read(self, board, signal):
        """
        Read from a named board and signal
        Leave decoding and validation to an external function.
        Returns a tuple of (rawsignalvalue, namedsignalvalue), for example:

        >>> tester.main_board.pump.read()
        'OFF'

        Names are extracted from the description document spreadsheet.
        Default names for 1 and 0 are 'ON' and 'OFF', active high unless otherwise specified.

        Note that there is a separation of concerns between different functions and methods:
        
          - tester.read does the actual call to the hardware
          - Callbuilder turns `board.signal.read()` into `tester.read(board,signal)`
          - board_and_signal() does validation as well as lookup into the description
        
        """

        lj_device, lj_signal, aliases = self.board_and_signal(READ, board, signal)

        try:
            hw_value = getattr(lj_device, lj_signal)
        except:
            print lj_device, board, signal, lj_signal
            raise APIError("There seems to be something wrong with the hardware description.")

        value = aliases[hw_value] if aliases else hw_value
        return value

    def write(self, board, signal, value):
        """
        Write a given value to a named board and signal.
        Leave decoding validation to an external function.
        Returns None. Example:

        >>> tester.main_board.door.write(OPEN)

        Note that there is a separation of concerns between different functions and methods:
        
          - tester.write(board, signal, value) does the actual call to the hardware
          - Callbuilder turns `board.signal.write(value)` into `tester.read(board,signal, value)`
          - board_and_signal() does validation as well as lookup into the description

        """

        lj_device, lj_signal, aliases = self.board_and_signal(WRITE, board, signal)
        hw_value = aliases[value] if aliases else value
        try:
            setattr(lj_device, lj_signal, hw_value)
        except:
            print board, signal, lj_signal, value, hw_value
            raise APIError("There seems to be something wrong with the hardware description.")

    def close(self):
        for device in self.devices.values():
            device.quit()

    def reset(self):
        for device in self.devices.values():
            pass
            # device.softReset()

    def board_and_signal(self, rw, board, signal):
        """
        Decodes/validates a given board/signal combination together with a WRITE or READ command.

        Walk down the argument list matching them against the description dict:
            - Extract board or throw APIError
            - Extract signal name or throw APIError
            - If trying to write to Input or read from Output, raise APIError
            - Find corresponding Labjack, make call
        """
        try:
            signals = self.description[board]
        except KeyError:
            raise APIError("'%s' is not a board mapped to a tester pin" % board)
        try:
            signalmapping = signals[signal]
        except KeyError:
            raise APIError("'%s' is not a signal mapped to a tester pin on board '%s'." % (signal, board))
        if signalmapping[RW] != rw:
            raise APIError("Wrong direction: '%s.%s' is a %s signal!" % (board, signal, signalmapping[RW]))

        aliases = signalmapping.get(ALIASES, None)
        lj_number, lj_signal = signalmapping[LABJACK]
        lj_device = self.devices[lj_number - 1]

        return lj_device, lj_signal, aliases


class Callbuilder():
    """
    Build a function call out of a chain of methods.

    >>> foo_bar = Callbuilder("bar", read=None, write=print_arglist)
    >>> foo_bar
    $yourinstance.bar
    >>> foo_bar.bing.nana
    $yourinstance.bar.bing.nana
    >>> foo_bar.bing.nana.write('booya')
    ['bar', 'bing', 'nana', 'booya']
    >>> foo_bar.bing.nana.write('booya', 34)
    ['bar', 'bing', 'nana', 'booya', 34]
    >>> foo2 = Callbuilder("bar", read=None, write=print_arglist)
    >>> foo2()
    Traceback (most recent call last):
    ...
    APIError: $yourinstance.bar is not callable

    """
    def __init__(self, method, read, write, more_methods=None):
        """
        The API call is created on passing the first method name...
        ...and optionally a list of follow up methods
        """
        self.first_method = method
        self.more_methods = more_methods if more_methods else []
        self.callmap = {
            "read": read,
            "write": write,
            }

    def __getattr__(self, method):
        """
        We dispatch the second, third, etc methods.
        We're agnostic about the API; we just build
        something like foo.read("a", "b", "c", "d",...)
        out of something like foo.a.b.c.d... .read() and let 
        the read or write function use the API description to
        decide whether it's a good or bad function call.
        """
        if method in self.callmap:
            f = self.callmap[method]
            chainedargs = chain([self.first_method], self.more_methods)
            return partial(f, *chainedargs)
        else:
            first_method = self.first_method
            more_methods = self.more_methods + [method]
            read, write = self.callmap['read'], self.callmap['write']
            return Callbuilder(first_method, read, write, more_methods)

    def __repr__(self):
        return ".".join(chain(["$yourinstance", self.first_method], self.more_methods))

    def __call__(self, *args):
        raise APIError("%s is not callable" % str(self))

    __str__ = __repr__

def print_arglist(*args, **kwargs):
    print([arg for arg in args])

class APIError(Exception):
    pass

if __name__ == '__main__':
    import doctest
    doctest.testmod()