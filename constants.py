
"""
Constants file.

Dear Reader:

I agree with you that the next trick is not especially pretty.
And that being 'clever' is generally not a good idea.

However, the tester description definition kept changing at the beginning,
and there were more constant to manage.

So, in order to save typing of quotation marks, I created "SYMBOLS",
which are a convention by which a constant's name and it's value are the same.

(c) 2013-2015 Javier Candeira and Planet Innovation.
"""

__symbolstrings = """
                    RW READ WRITE  
                    ANALOG DIGITAL
                    LABJACK 
                    ALIASES HI_ALIAS LO_ALIAS
                    ON OFF 
                    OPEN CLOSED 
                    ABSENT PRESENT 
                    DI1 DI2 AI1 AI2
                    DO1 DO2 AO1 AO2
                    """
SYMBOLS = {s:s for s in __symbolstrings.split()}
locals().update(SYMBOLS)

DEFAULT_ALIASES = {1: ON, 0: OFF, ON: 1, OFF: 0}
INPUTS = [DI1, DI2, AI1, AI2]
OUTPUTS = [DO1, DO2, AO1, AO2]
ALLSIGNALS = OUTPUTS + INPUTS
