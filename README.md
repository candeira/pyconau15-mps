
# Python Metaprogramming with Spreadsheets 1: Reflection

## Tutorial Introduction

This is the study material for tutorial that was given at Pycon Austraia 2015. There are also slides and a video:

 - [_Python Metaprogrammimng with Spreadsheets_](http://hiperactivo.com/talks/pyconau15/mps): the slides.
 - [_Python Metaprogrammimng with Spreadsheets_](https://www.youtube.com/watch?v=DmLHqOvbG9M): the video.

The title here has an added "1: Reflection" because there was too much material. However, this is really the whole of the tutorial as was given at Pycon AU. The part on code generation was only displayed for five minutes at the end.

Note that the version of the tutorial given at Pycon AU 2105 lives in its own branch, pyconau15. The master branch contains modified code as the tutorial evolves in subsequent presentations. So if you plan to follow the above video, make sure you've checked out the pyconau15 branch.

## Pre-requirements

This tutorial assumes a certain previous level of skills and knowledge:

 - you already have Python 2.7 installed.
 - you can use the command line.
 - you know how to use virtualenvs ([virtualenv tutorial](http://docs.python-guide.org/en/latest/dev/virtualenvs/)).
 - you know how to use iPython Notebook ([iPython notebook tutorial](https://ipython.org/ipython-doc/3/notebook/notebook.html#notebook-user-interface)).

You don't need to be a super-expert at any of this, but if you've never used iPython Notebook or virtualenvs, you'll have to find out about them somewhere else. I've provided links.

## Installing and launching the software

After you've cloned this repo:

 0. check out the `pyconau15` branch.
 0. make a new python 2.7 virtualenv.
 0. open a terminal and activate your virtualenv.
 0. install the requirements with `pip install -r requirements.txt`
 0. start the notebook with `ipython notebook` 

This will launch the notebook server and open it on your default browser.

## Following the tutorial

The notebook you want is called **MetaProgramming With Spreadsheets 1 - Reflection.ipynb**. Click on it.

In a different window, open the [tutorial video from Pycon AU 2015]() and watch until I start interacting with the iPython notebook.

Interact yourself with the iPython notebook until you're happy that you understand the previous point made in the video before going to the next step.

Reading the code in `constants.py` and `instrument_tester_api.py` will help, but the brunt of the work is done in `tester.py`. The comments have been extended for the tutorial.

This is how the contents of the tutorial are assembled at the end, and how the separation of concerns is achieved in the `Tester` class in `tester.py`:

 - `Tester.read()` does the actual call to the hardware
 - `Callbuilder` turns `board.signal.read()` into `tester.read(board,signal)`
 - `board_and_signal()` does validation as well as lookup into the description

## Extra credit

You can ignore the `minijack.py` and `minijackgui.py` modules. They were required for the demonstration, but they don't contain any code demonstrated on, except that...

`minijack.py` contains a good example of using `__setattr__` and `__getattr__` to provide a pythonic API. So you may want to look inside it.

## Notes on the labjack API interface

I didn't have a device to hand while writing the tutorial, so the LabJack API has been not only deliberately simplified, but also unvoluntarily distorted. Should you need to interface with an actual labjack, you'll need to map the calls so they work with the device's API. Please ask me if you need help.

## Acknowledgements
 - Michael Liao, from whose githubpy (https://github.com/michaelliao/githubpy) I learnt the technique for reflection.
 - Graeme and Adrian at [Planet Innovation](http://www.planetinnovation.com.au) and [Jenn](https://www.linkedin.com/pub/jennifer-scheuerell/21/870/458) at Sound Data for support</a>.
 - The communities of Melbourne and Madrid Python User Groups for helping workshop this tutorial and improving it with questions, advice and criticism.
