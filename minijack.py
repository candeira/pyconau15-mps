#!/usr/bin/env python
# coding: utf-8"""minijack.py and minijackgui.py

"""
This is a GUI simulator of a small LabJack with 2 inputs and 2 outputs
of each, digital and analog.

Because tkinter is a mess with ipython, I'm creating it as a client-server
application. It's all very ad-hoc:

- minijack.py is the client module that gets used by applications. It clones the
  labjack SDK interface more or less:

>>> from minijack import MiniJack
>>> mj = MiniJack()
>>> mj.setDOState(0, 1)
>>> mj.getAIState(1)
7

- minijackgui.py is ran as a sever via python subprocess. It is a tkinter
  application that opens a unix socket for minijack.py's MiniJack object to
  connect to. Messages are sent and received via a filelike thread on both
  sides, and used to update values on either end.

This uses, in a very modified state, code from the now almost unrecognisable
recipe at:
http://code.activestate.com/recipes/82965-threads-tkinter-and-asynchronous-io/
Originally created by Jacob Hallén, AB Strakt, Sweden. 2001-10-17

For this reason I can't put it in the public domain, but yeah, I think you can
use it.

"""

import logging
import os
import Queue
import socket
import subprocess
import signal
import sys
import time
import threading
from threading import Timer

from constants import *

def get_logger(name):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.CRITICAL)
    # create a file handler
    handler = logging.FileHandler('minijack.log')
    handler.setLevel(logging.CRITICAL)
    # create a logging format
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(handler)
    return logger

class MiniJack(object):
    """
    This is just the sdk module one imports to drive the MiniJackGUi
    """

    def __init__(self):
        self.logger = get_logger(__name__)
        self.logger.info("sdk initialising the minijack object")
        self.address = "/tmp/minijack"
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sockfile = None
        self.other_thread = None
        self.running = True

        # the eight signals
        self._DI1 = 0
        self._DI2 = 0 
        self._DO1 = 0 
        self._DO2 = 0 
        self._AI1 = 0 
        self._AI2 = 0 
        self._AO1 = 0 
        self._AO2 = 0 

    def start_gui(self):
        self.logger.info("sdk just before starting the gui server process")
        self.killall_with("minijackgui")
        self.logger.info("sdk now really starting the gui server process")
        this_dir = os.path.dirname(os.path.realpath(__file__))
        minijackgui_path = os.path.join(this_dir, "minijackgui.py")
        FNULL = open(os.devnull, 'w')
        gui = subprocess.Popen(minijackgui_path, stdout=FNULL, stderr=subprocess.STDOUT)
        time.sleep(2)
        self.connect()
        return gui

    def killall_with(self, pattern):
        self.logger.info("sdk killing all previous gui servers just in case")
        FNULL = open(os.devnull, 'w')
        p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE, stderr=FNULL)
        out, err = p.communicate()
        for line in out.splitlines():
            if pattern in line:
                pid = int(line.split(None, 1)[0])
                os.kill(pid, signal.SIGKILL)
        
    def connect(self):
        self.logger.info("sdk connecting to %s" % self.address)
        try:
            self.sock.connect(self.address)
            self.logger.info("sdk succeeded at connecting to %s" % self.address)
        except socket.error, msg:
            self.logger.info("sdk suffered a socket.error dammit")
            self.logger.info(msg)
            sys.exit(1)
        self.sockfile = self.sock.makefile()
        # Start the worker thread, which receives from the connection
        self.other_thread = threading.Thread(target=self.worker)
        self.other_thread.daemon = True
        self.other_thread.start()
        
    def worker(self):
        self.logger.info("sdk worker thread started")
        while self.running:
            self.logger.info("sdk worker thread running")
            try:
                self.logger.info("sdk worker reading from socket")
                for line in self.readlines(self.sockfile): 
                    self.running = self.process_message(line)
                    if not self.running:
                        break
            except KeyboardInterrupt:
               self.quit()
            except Exception, e: 
                # this was so I could close when socket died; it doesn't work
                # I'd have to do it checking if it restarts too often
                # but I better move on now
                # TODO, however.
                if "'NoneType' object has no attribute 'recv'" in str(e):
                    self.logger.info("socket died, closing app")
                    break
                elif "unexpected EOF while parsing" in str(e):
                    self.logger.info("empty message received, weird!" )
                else:
                    self.logger.info(e)
        self.logger.info("worker thread finished")
    
    def __setattr__(self, name, value):
        if name in INPUTS:
            raise Exception("Signal %s is an input, you can't set it here!" % name)
        elif name in OUTPUTS:
            msg = (name, value)
            self.send_message(msg)
            super(MiniJack, self).__setattr__("_" + name, int(value))
        else:
            super(MiniJack, self).__setattr__(name, value)

    def __getattr__(self, name):
        if name in ALLSIGNALS:
            name = "_" + name
        return getattr(self, name)

    @staticmethod
    def readlines(fh):
        while True:
            data = fh.readline()
            if not data:
                return
            yield data

    def send_message(self, msg):
        self.logger.info("sdk sending message: %s" % str(msg))
        line = str(msg) + "\n"
        self.sockfile.write(line)
        self.sockfile.flush()
        
    def process_message(self, msg):
        self.logger.info("sdk processing received message: %s" % str(msg),)
        running = True
        if msg:
            signal, value = eval(msg)
            value = int(value)
            if signal == "QUIT":
                running = False
            elif signal in INPUTS:
                setattr(self, "_" + signal, value)
        return running

    def setDOState(self, signalnumber, value):
        setattr(self, "DO" + str(signalnumber), value)

    def setAOState(self, signalnumber, value):
        setattr(self, "AO" + str(signalnumber), value)

    def getDIState(self, signalnumber):
        return getattr(self, "DI" + str(signalnumber))

    def getAIState(self, signalnumber):
        return getattr(self, "AI" + str(signalnumber))


    def quit(self):
        self.running = False
        try:
            self.send_message(("QUIT", True))
        except:
            pass
        self.gui.terminate()
        
    def __del__(self):
        try:
            self.send_message(("QUIT", True))
        except:
            pass
        if self.running:
            self.quit()
        
        
