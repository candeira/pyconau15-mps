#!/usr/bin/env python
# coding: utf-8

"""minijack.py and minijackgui.py

This is a GUI simulator of a small LabJack with 2 inputs and 2 outputs
of each, digital and analog.

Because tkinter is a mess with ipython, I'm creating it as a client-server
application. It's all very ad-hoc:

- minijack.py is the client module that gets used by applications. It clones the
  labjack SDK interface more or less:

>>> from minijack import MiniJack
>>> mj = MiniJack()
>>> mj.setDOState(0, 1)
>>> mj.getAIState(1)
7

- minijackgui.py is ran as a sever via python subprocess. It is a tkinter
  application that opens a unix socket for minijack.py's MiniJack object to
  connect to. Messages are sent and received via a filelike thread on both
  sides, and used to update values on either end.

This uses, in a very modified state, code from the now almost unrecognisable
recipe at:
http://code.activestate.com/recipes/82965-threads-tkinter-and-asynchronous-io/
Originally created by Jacob Hallén, AB Strakt, Sweden. 2001-10-17

For this reason I can't put it in the public domain, but yeah, I think you can
use it.

"""

import os
import Queue
import socket
import sys
import time
import threading
from threading import Timer

try:
    import tkinter as tk
except:
    import Tkinter as tk
import pygubu

from minijack import get_logger
from constants import *

# from https://gist.github.com/walkermatt/2871026
def debounce(wait):
    """ Decorator that will postpone a functions
        execution until after wait seconds
        have elapsed since the last time it was invoked. """
    def decorator(fn):
        def debounced(*args, **kwargs):
            def call_it():
                fn(*args, **kwargs)
            try:
                debounced.t.cancel()
            except(AttributeError):
                pass
            debounced.t = Timer(wait, call_it)
            debounced.t.start()
        return debounced
    return decorator

class MiniJackGui(pygubu.TkApplication):
    """
    Launch the main part of the GUI and the worker thread. check_queues_periodically and
    endApplicat fsion could reside in the GUI part, but putting them here
    means that you have all the thread controls in a single place.
    """

    def __init__(self, master=None):
        """
        We don't do anything here, because we let parent do it. As seen on:
        https://github.com/alejandroautalan/pygubu/blob/2a4105587814e83/pygubu/__init__.py
        """
        pygubu.TkApplication.__init__(self, master)

    def _init_before(self):
        self.address = "/tmp/minijack"
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.logger = get_logger("MiniJackGui")
        self.sockfile = None
        self.serve()

    def _create_ui(self):
        # create a GUI
        self.builder = builder = pygubu.Builder()
        builder.add_from_file('minijack.ui')
        self.mainwindow = builder.get_object('MainWindow', self.master)
        builder.connect_callbacks(self)

    def _init_after(self):
        """
        Init vars and start the asynchronous threads. We are in the main
        (original) thread of the application, which will later be used by
        the GUI. We spawn a new thread for the worker.
        """
        self.set_title("MiniJack")

        # the eight signals for a MiniJack:
        self.DI1 = self.builder.get_variable("DI1")
        self.DI2 = self.builder.get_variable("DI2")
        self.DO1 = self.builder.get_variable("DO1")
        self.DO2 = self.builder.get_variable("DO2")
        self.AI1 = self.builder.get_variable("AI1")
        self.AI2 = self.builder.get_variable("AI2")
        self.AO1 = self.builder.get_variable("AO1")
        self.AO2 = self.builder.get_variable("AO2")

        # the starting values for the inputs
        self.DI1_value = self.DI1.get()
        self.DI2_value = self.DI2.get()
        self.AI1_value = self.AI1.get()
        self.AI2_value = self.AI2.get()

        # Start the worker thread
        # anything
        self.running = True
        self.other_thread = threading.Thread(target=self.worker)
        self.other_thread.daemon = True
        self.other_thread.start()

    def serve(self):
        self.logger.info("gui serving from %s" % self.address)
        try:
            os.remove(self.address)
        except OSError:
            # file already deleted
            pass
        try:
            self.sock.bind(self.address)
            self.sock.listen(1)
            conn, addr = self.sock.accept()
            self.logger.info("gui: connection accepted!")
        except socket.error, msg:
            self.logger.info(msg)
            sys.exit(1)
        self.sockfile = conn.makefile("rw", 2048)

    @debounce(0.3)
    def on_AI1_changed(self, value):
        msg = ("AI1", value)
        self.send_message(msg)

    @debounce(0.3)
    def on_AI2_changed(self, value):
        msg = ("AI2", value)
        self.send_message(msg)

    def on_DI1_changed(self):
        value = self.DI1.get()
        msg = ("DI1", value)
        self.send_message(msg)

    def on_DI2_changed(self):
        value = self.DI2.get()
        msg = ("DI2", value)
        self.send_message(msg)

    @staticmethod
    def readlines(fh):
        while True:
            data = fh.readline()
            if not data:
                return
            yield data

    def worker(self):
        self.logger.info("gui worker thread started")
        while self.running:
            self.logger.info("gui worker thread running")
            try:
                self.logger.info("gui worker reading from socket")
                for line in self.readlines(self.sockfile): 
                    self.running = self.process_message(line)
                    if not self.running:
                        break
            except Exception, e: 
                # this was so I could close when socket died; it doesn't work
                # I'd have to do it checking if it restarts too often
                # but I better move on now
                # TODO, however.
                self.logger.info(e)
                if "'NoneType' object has no attribute 'recv'" in str(e):
                    self.logger.info("socket died, closing app")
                    break
                elif "unexpected EOF while parsing" in str(e):
                    self.logger.info("empty message received, weird!" )
                else:
                    pass
        self.logger.info("gui worker thread finished")
        self.quit()

    def send_message(self, msg):
        self.logger.info("gui sending message: %s" % str(msg))
        line = str(msg) + "\n"
        self.sockfile.write(line)
        self.sockfile.flush()
        
    def process_message(self, msg):
        self.logger.info("gui processing received message: %s" % str(msg),)
        running = True
        if msg:
            signal, value = eval(msg)
            if signal == "QUIT":
                self.quit()
                running = False
            tk_variable = getattr(self, signal)
            tk_variable.set(value)
        return running

    # Frustratingly, none of this works
    # we'll just shut down the process from the calling library
    
    def on_close_execute(self):
        """Returns boolean as per the pygubu api, see above url"""
        self.running = False
        try:
            self.send_message(("QUIT", True))
            self.sockfile.close()
        except:
            pass
        return True

    def quit(self):
        try:
            self.send_message(("QUIT", True))
            self.running = False
        except:
            pass
        pygubu.TkApplication.quit(self)
        del(self)

    def __del__(self):
        super(MiniJackGui, self).__del__(self)
        
if __name__ == '__main__':
    try:
        root = tk.Tk()
        app = MiniJackGui(root)
        app.run()
    except KeyboardInterrupt:
        try:
            app.quit()
        except:
            pass
