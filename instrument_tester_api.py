#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""
Utility functions for making a hardware tester description dictionaryfrom a csv description of LabJack connections.

(c) 2013-2015 Javier Candeira and Planet Innovation.
"""

from pprint import pformat
from constants import *
from collections import defaultdict
from os.path import join, dirname
from pprint import pformat
import csv

CSV_DESCRIPTION_RELPATH = "instrument_tester_API.csv"

def make_tester_description(filepath=CSV_DESCRIPTION_RELPATH):
    """Given a filename containing the hardware description,
    return a dictionary containing the description. 

    Input file is:
        - Saved from instrument_tester_API.ods spreadsheet
        - As CSV, with tabs for separators, " for quotechar.
    """

    with open(filepath, 'r') as csvfile:
        descreader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
        board_name = None
        description = defaultdict(dict)
        for line in descreader:
            # clean up whitespace
            row = {header.strip(): cell.strip() for header, cell in line.items()}
            # board names are only listed once per board
            if row["board_name"]:
                board_name = row["board_name"]
                continue
            # we only output those signals that have a signal_name defined
            signal_name = row["signal_name"]
            if signal_name:
                signal_description = make_signal_description(row)
                # example: tester.main_board.temp_sensor is described by
                # description['main_board']['temp_sensor']
                description[board_name][signal_name] = signal_description

    return dict(description)

def make_signal_description(row):
    """
    Input: a dictionary containing:
        - keys: the headers from the spreadsheet indicating what each column is
        - values: the line from the .csv file corresponding to a signal
    Output: a description of a given tester signal, to be used by the tester API.

    Example input:

    {'LABJACK1 PORT': '', 'LABJACK2 PORT': '2_DI1', 'signal_name': 'heater',
     'HI_ALIAS', 'OFF', 'LO_ALIAS': 'ON'}  

    Example output:
                {
                    'signal_name': 'heater'
                    'RW': 'READ',
                    'LABJACK': (2, 'DI2'),
                    'ALIASES': {
                        1: 'PRESENT',
                        0: 'ABSENT'
                    }
                }

    In the case where there are no LO_ALIAS and HI_ALIAS, the default aliases
    are ON for HI or 1, OFF for LO or 0.
    """
    parsed_row = {}
    # only needed for error reporting:
    signal_name = row['signal_name']    

    # LABJACK first:
    labjack1, labjack2 = row['LABJACK1 PORT'], row['LABJACK2 PORT']
    if labjack1 and labjack2:
        raise APIError("Two labjack ports defined for %s; one maximum." % signal_name)
    labjack = labjack1 or labjack2
    if not labjack:
        raise APIError("No labjack port defined for %s; one required." % signal_name)
    lj_number, lj_port = labjack.split("_")
    parsed_row[LABJACK] = (int(lj_number), lj_port)

    # READ or WRITE port?
    operation = {"I": READ, "O": WRITE}[lj_port[1]]
    parsed_row[RW] = operation

    # ANALOG or DIGITAL?
    # Not needed later, just here for whether aliases exist
    signal_type = {"D": DIGITAL, "A": ANALOG}[lj_port[0]]

    # ALIASES at last
    hi_alias, lo_alias = row[HI_ALIAS], row[LO_ALIAS]
    if bool(hi_alias) ^ bool(lo_alias):
        # only one alias defined: this is an error
        raise APIError("Only one alias defined for %s; two required." % signal_name)
    if hi_alias and lo_alias:
        if signal_type == DIGITAL:
            parsed_row[ALIASES] = {1: hi_alias, 0: lo_alias, hi_alias: 1, lo_alias: 0}
        else:
            raise APIError("Analog signal %s should not have aliases" % signal_name)
    else:
        if signal_type == DIGITAL:
            parsed_row[ALIASES] = DEFAULT_ALIASES
        else:
            pass # analog signals should not have aliases.

    return parsed_row

class APIError(Exception):
    pass

